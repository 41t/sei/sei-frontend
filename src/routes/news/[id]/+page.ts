import type { PageLoad } from './$types';
import { axiosInstance } from '$lib/config';
import { error } from '@sveltejs/kit';

export const load: PageLoad = async ({ params }: any) => {
  const { data, status } = await axiosInstance.get(`/news/${params.id}`);

  if (status === 200) {
    if (data.attachments?.length) {
      if (data.attachments?.length) {
        const attachments = [];
        for (const attachment of data.attachments) {
          const { data, status, headers } = await axiosInstance.get(`/attachment/${attachment.attachmentId}`);
          if (status === 200) {
            const file = new File([data], attachment.attachmentId, { type: headers['content-type'] });
            attachments.push({ file, id: attachment.attachmentId });
          }
        }
        data.attachments = attachments;
      }
    }
    return { news: data };
  }

  throw error(404);
};