import type { PageLoad } from './$types';
import { axiosInstance } from '$lib/config';

export const load: PageLoad = async () => {
  const endpoints = ['news', 'journal', 'school', 'project', 'research', 'sno'];

  let result: any = {};

  async function getData(endpoint: string) {
    try {
      const { data, status } = await axiosInstance.get(`/${endpoint}?page=1&size=3`);
      if (status === 200) {
        if (data?.data.length) {
          data.data = data.data.map(async (item: any) => {
            // item.createdAt = new Date(item.createdAt);
            // item.updatedAt = new Date(item.updatedAt);
            // item.deletedAt = new Date(item.deletedAt);
            return item;
          });

          data.data = await Promise.all(data.data);
          return data;
        } else {
          return data;
        }
      }

      return [];
    } catch (error) {
      console.error(`Ошибка получения данных из ${endpoint}:`, error);
      return null;
    }
  }

  for (const endpoint of endpoints) {
    result[endpoint] = await getData(endpoint);
  }

  return result;
};