import type { PageLoad } from './$types';
import { axiosInstance } from '$lib/config';
import { error } from '@sveltejs/kit';

export const load: PageLoad = async ({ params }: any) => {
    const res = await axiosInstance.get(`/user/${params.id}`);

    if (res.status === 200) return { user: res.data };
    throw error(404);
};