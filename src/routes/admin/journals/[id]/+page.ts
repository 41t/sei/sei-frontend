import type { PageLoad } from './$types';
import { axiosInstance } from '$lib/config';

export const load: PageLoad = async ({ params }) => {
  try {
    const { data, status } = await axiosInstance.get(`/journal/${params.id}`);
    if (status === 200) return { journal: data };
  } catch (error) {
    console.error(`Ошибка получения данных из journal:`, error);
  }

  return { journal: null };
};