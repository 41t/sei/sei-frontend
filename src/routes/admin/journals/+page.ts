import type { PageLoad } from './$types';
import { axiosInstance } from '$lib/config';

export const load: PageLoad = async () => {
  try {
    const { data, status } = await axiosInstance.get(`/journal`);
    if (status === 200) return { journal: data };
  } catch (error) {
    console.error(`Ошибка получения данных из journal:`, error);
  }

  return { journal: [] };
};