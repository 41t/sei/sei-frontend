import type { PageLoad } from './$types';
import { axiosInstance } from '$lib/config';

export const load: PageLoad = async () => {
  try {
    const { data, status } = await axiosInstance.get(`/school`);
    if (status === 200) return { school: data };
  } catch (error) {
    console.error(`Ошибка получения данных из school:`, error);
  }

  return { school: [] };
};