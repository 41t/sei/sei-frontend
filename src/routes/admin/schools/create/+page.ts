import type { PageLoad } from './$types';
import { axiosInstance } from '$lib/config';

export const load: PageLoad = async () => {
  try {
    const { data, status } = await axiosInstance.get(`/scientificschool`);

    if (status === 200) return { scientificSchools: data.data };
  } catch (error) {
    console.error(`Ошибка получения данных из scientific schools:`, error);
  }

  return { scientificSchools: null };
};