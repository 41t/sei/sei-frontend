import type { PageLoad } from './$types';
import { axiosInstance } from '$lib/config';

export const load: PageLoad = async ({ params }) => {
  try {
    const { data, status } = await axiosInstance.get(`/school/${params.id}`);
    const { data: scientificSchoolData, status: scientificSchoolStatus } = await axiosInstance.get(`/scientificschool`);

    if (status === 200 && scientificSchoolStatus === 200) {
      return {
        school: data,
        scientificSchools: scientificSchoolData.data
      };
    }
  } catch (error) {
    console.error(`Ошибка получения данных из school:`, error);
  }

  return { school: null };
};