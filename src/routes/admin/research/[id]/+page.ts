import type { PageLoad } from './$types';
import { axiosInstance } from '$lib/config';

export const load: PageLoad = async ({ params }) => {
  try {
    const { data, status } = await axiosInstance.get(`/research/${params.id}`);
    const { data: categoriesData, status: categoriesStatus } = await axiosInstance.get(`/researchcategory`);
    if (status === 200 && categoriesStatus === 200) return { research: data, categories: categoriesData.data };
  } catch (error) {
    console.error(`Ошибка получения данных из research:`, error);
  }

  return { research: null, categories: null };
};