import type { PageLoad } from './$types';
import { axiosInstance } from '$lib/config';

export const load: PageLoad = async () => {
  try {
    const { data, status } = await axiosInstance.get(`/research`);
    if (status === 200) return { research: data };
  } catch (error) {
    console.error(`Ошибка получения данных из research:`, error);
  }

  return { research: [] };
};