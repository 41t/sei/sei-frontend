import type { PageLoad } from './$types';
import { axiosInstance } from '$lib/config';

export const load: PageLoad = async ({ params }) => {
  try {
    const { data, status } = await axiosInstance.get(`/researchcategory`);
    if (status === 200) return { categories: data.data };
  } catch (error) {
    console.error(`Ошибка получения данных из research category:`, error);
  }

  return { research: null };
};