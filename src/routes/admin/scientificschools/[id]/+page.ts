import type { PageLoad } from './$types';
import { axiosInstance } from '$lib/config';

export const load: PageLoad = async ({ params }) => {
  try {
    const { data, status } = await axiosInstance.get(`/scientificschool/${params.id}`);

    if (status === 200) return { scientificSchool: data };
  } catch (error) {
    console.error(`Ошибка получения данных из scientific school:`, error);
  }

  return { scientificSchool: null };
};