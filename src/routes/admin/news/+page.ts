import type { PageLoad } from './$types';
import { axiosInstance } from '$lib/config';

export const load: PageLoad = async () => {
  try {
    const { data, status } = await axiosInstance.get(`/news`);
    if (status === 200) return { news: data };
  } catch (error) {
    console.error(`Ошибка получения данных из news:`, error);
  }

  return { news: [] };
};