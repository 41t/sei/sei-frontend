import type { PageLoad } from './$types';
import { axiosInstance } from '$lib/config';

export const load: PageLoad = async () => {
  try {
    const { data, status } = await axiosInstance.get(`/sno`);
    if (status === 200) return { ssa: data };
  } catch (error) {
    console.error(`Ошибка получения данных из ssa:`, error);
  }

  return { ssa: [] };
};