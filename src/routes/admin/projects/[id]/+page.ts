import type { PageLoad } from './$types';
import { axiosInstance } from '$lib/config';

export const load: PageLoad = async ({ params }) => {
  try {
    const { data, status } = await axiosInstance.get(`/project/${params.id}`);
    if (status === 200) return { project: data };
  } catch (error) {
    console.error(`Ошибка получения данных из project:`, error);
  }

  return { project: null };
};