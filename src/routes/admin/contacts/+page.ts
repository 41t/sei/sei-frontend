import type { PageLoad } from './$types';
import { axiosInstance } from '$lib/config';

export const load: PageLoad = async () => {
  try {
    const { data, status } = await axiosInstance.get(`/contact`);
    if (status === 200) return { contact: data };
  } catch (error) {
    console.error(`Ошибка получения данных из contact:`, error);
  }

  return { contact: [] };
};