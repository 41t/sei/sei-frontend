import type { PageLoad } from './$types';
import { axiosInstance } from '$lib/config';

export const load: PageLoad = async ({ params }) => {
  try {
    const { data, status } = await axiosInstance.get(`/contact/${params.id}`);
    if (status === 200) return { contact: data };
  } catch (error) {
    console.error(`Ошибка получения данных из contact:`, error);
  }

  return { contact: null };
};