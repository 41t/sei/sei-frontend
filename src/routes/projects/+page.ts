import type { PageLoad } from './$types';
import { axiosInstance } from '$lib/config';

export const load: PageLoad = async ({ url }) => {
  try {
    const page = Number(url.searchParams.get('page')) || 1,
      { data, status } = await axiosInstance.get(`/project?page=${page}&size=9`);

    if (status === 200) {
      return { project: data, page };
    }
  } catch (error) {
    console.error(`Ошибка получения данных из project:`, error);
  }

  return { project: [] };
};