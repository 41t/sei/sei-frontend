import { axiosInstance } from '$lib/config';
import type { LayoutLoad } from './$types';

export const load: LayoutLoad = async () => {
  try {
    const res = await axiosInstance.get('/auth/profile');

    if ([200].includes(res.status)) return { user: res.data };

    return { user: undefined };
  } catch {}
};