import axios from 'axios';
import type { AxiosRequestConfig } from 'axios';
import { PUBLIC_API_URL } from '$env/static/public';

export const axiosInstance = axios.create({
  baseURL: PUBLIC_API_URL,
  withCredentials: true
} as AxiosRequestConfig);

axiosInstance.defaults.headers.post['Access-Control-Allow-Origin'] = '*';