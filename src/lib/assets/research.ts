interface Research {
  id: number;
  name: string;
  category: string;
}

export const research: Research[] = [{
  id: 0,
  name: 'Благородные стремления не спасут: жизнь прекрасна',
  category: 'Математика'
}, {
  id: 1,
  name: 'Благородные стремления не спасут: жизнь прекрасна',
  category: 'Математика'
}, {
  id: 2,
  name: 'Благородные стремления не спасут: жизнь прекрасна',
  category: 'Математика'
}];