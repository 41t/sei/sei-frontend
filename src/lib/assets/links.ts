export interface Link {
  name: string;
  href: string;
}

export const links: Link[] = [{
  name: 'Новости',
  href: '/news'
}, {
  name: 'Журналы',
  href: '/journals'
}, {
  name: 'Исследователям',
  href: '/research'
}, {
  name: 'Научные школы',
  href: '/schools'
}, {
  name: 'Проекты',
  href: '/projects'
}, {
  name: 'СНО',
  href: '/ssa'
}, {
  name: 'Контакты',
  href: '/contacts'
}];

export const adminLinks: Link[] = [{
  name: 'Новости',
  href: '/admin/news'
}, {
  name: 'Журналы',
  href: '/admin/journals'
}, {
  name: 'Исследователям',
  href: '/admin/research'
}, {
  name: 'Научные школы',
  href: '/admin/scientificschools'
}, {
  name: 'Школы',
  href: '/admin/schools'
}, {
  name: 'Проекты',
  href: '/admin/projects'
}, {
  name: 'СНО',
  href: '/admin/ssa'
}, {
  name: 'Пользователи',
  href: '/admin/users'
}, {
  name: 'Контакты',
  href: '/admin/contacts'
}];