import type { Link } from './links.ts';

export const journals: Link[] = [{
  name: 'Журнал 1',
  href: '/'
}, {
  name: 'Журнал 2',
  href: '/'
}, {
  name: 'Журнал 3',
  href: '/'
}, {
  name: 'Журнал 4',
  href: '/'
}];