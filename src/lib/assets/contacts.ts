interface Contact {
  name: string;
  phone: string;
  position: string;
  email: string;
}

export const contacts: Contact[] = [{
  name: 'Румянцева Вера Святославовна',
  phone: '+7 (999) 999-99-99',
  position: 'Директор',
  email: 'test@example.com'
}, {
  name: 'Румянцева Вера Святославовна',
  phone: '+7 (999) 999-99-99',
  position: 'Директор',
  email: 'test@example.com'
}, {
  name: 'Румянцева Вера Святославовна',
  phone: '+7 (999) 999-99-99',
  position: 'Директор',
  email: 'test@example.com'
}, {
  name: 'Румянцева Вера Святославовна',
  phone: '+7 (999) 999-99-99',
  position: 'Директор',
  email: 'test@example.com'
}, {
  name: 'Румянцева Вера Святославовна',
  phone: '+7 (999) 999-99-99',
  position: 'Директор',
  email: 'test@example.com'
}, {
  name: 'Румянцева Вера Святославовна',
  phone: '+7 (999) 999-99-99',
  position: 'Директор',
  email: 'test@example.com'
}];