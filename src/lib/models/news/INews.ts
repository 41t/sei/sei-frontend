export interface INews {
  attachmentId: number;
  id: number;
  name: string;
  text: string;
  anons: string;
  createdAt: string;
  updatedAt: string;
  deletedAt: string;
  userId: number;
}
